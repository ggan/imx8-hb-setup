# imx8-hb-setup

General kernel configuration workflow for imx8-hb

# Linux Kernel Compile on IMX8

Original kernel `4.14.104` was from SR official debian image sr-imx8-debian-buster-20190305-cli-sdhc.img.xz . However the compiled kernel is missing the core `CONFIG_VIRTUALIZATION` feature and thus `/dev/kvm` cannot be found. A kernel recompilation with `CONFIG_VIRTUALIZATION` flag set is therefore required.ways

Following are steps to compile kernel using 2 options:
* custom compile
* buildroot system while using the rootfs created by Solidrun

# Setup the Cross Compile Toolchain
Download linaro gcc
```
```

Setup the shell vars
```
export ARCH=arm64
export CROSS_COMPILE=/home/gary/imx8-dev/toolchain/gcc-linaro-7.3.1-2018.05-i686_aarch64-linux-gnu/bin/aarch64-linux-gnu-
```

# Method 1: Build everything from scratch
```
1. Download imx-mkimage
# Compile ATF
sudo apt-get install lib32z1

# Compile Uboot
sudo apt-get install bison flex

# Compile Linux Kernel
sudo apt-get install libncurses5-dev libncursesw5-dev

git clone https://github.com/SolidRun/linux-stable -b linux-4.9.y-imx8

# Putting all together
mkdir -p $ROOTDIR/images/tmp/
cd $ROOTDIR/images
dd if=/dev/zero of=tmp/part1.fat32 bs=1M count=28
mkdosfs tmp/part1.fat32
mcopy -i tmp/part1.fat32 /home/gary/imx8-dev/build/linux-imx/linux-stable/arch/arm64/boot/Image ::/Image
mcopy -s -i tmp/part1.fat32 /home/gary/imx8-dev/build/linux-imx/linux-stable/arch/arm64/boot/dts/freescale/fsl-imx8mq-hummingboard-pulse.dtb ::/

dd if=/dev/zero of=microsd.img bs=1M count=101
dd if=/home/gary/imx8-dev/build/imx-mkimage/iMX8M/flash.bin of=microsd.img bs=1K seek=33 conv=notrunc
parted --script microsd.img mklabel msdos mkpart primary 2MiB 30MiB mkpart primary 30MiB 60MiB
dd if=tmp/part1.fat32 of=microsd.img bs=1M seek=2 conv=notrunc
dd if=/home/gary/imx8-dev/build/buildroot/output/images/rootfs.ext2 of=microsd.img bs=1M seek=30 conv=notrunc
```



# Method 2: Buildroot
First, download and flash the debian OS image file from SR official repo. Once the sdcard is flashed, we will then replace its kernel with our custom built kernel, while still using its rootfs.

Requirements
```
sudo apt-get install texinfo
```

Clone the buildroot repo and copy the default buildroot config tailored for the hbpulse.
```
git clone https://github.com/SolidRun/buildroot.git --branch sr-latest
cd buildroot
make solidrun_imx8mq_hbpulse_defconfig
```

`make` will copy the buildroot environment config for hummingboard. The config contains the necessary link to download linux kernel, ATF, uboot, imx8 signed binaries, etc.

Snippet of the solidrun_imx8mq_hbpulse_defconfig buildroot config file. Note the custom repo that links to Solidrun repo.

<>

Since we want to recompile kernel with `CONFIG_VIRTUALIZATION` set, we have to use `linux-menuconfig` to configure the kernel. The following will download the 4.14.104 kernel and enter a CLI-like GUI interface for kernel configuration
```
make linux-menuconfig
```

Navigate to `Virtualization` and tick `Kernel-based Virtual Machine (KVM) support`.

example of `.config` after the option is selected.
```
...
CONFIG_HAVE_KVM_IRQ_ROUTING=y
CONFIG_HAVE_KVM_EVENTFD=y
CONFIG_KVM_MMIO=y
CONFIG_HAVE_KVM_MSI=y
CONFIG_HAVE_KVM_CPU_RELAX_INTERCEPT=y
CONFIG_KVM_VFIO=y
CONFIG_HAVE_KVM_ARCH_TLB_FLUSH_ALL=y
CONFIG_KVM_GENERIC_DIRTYLOG_READ_PROTECT=y
CONFIG_KVM_COMPAT=y
CONFIG_VIRTUALIZATION=y
CONFIG_KVM=y
CONFIG_KVM_ARM_HOST=y
CONFIG_KVM_ARM_PMU=y
CONFIG_VHOST_NET=m
CONFIG_VHOST=m
# CONFIG_VHOST_CROSS_ENDIAN_LEGACY is not set
...
```

Note: This particular kernel only support built-in or statically link KVM support instead of loadable module. So we do not have the manually load the kvm.ko at runtime. The status can be checked later on after booting into kernel via `dmesg | grep kvm`.

Exit and save the changes. A `.config` file will be stored in `output/build/linux-linux-4.14.y-nxp/.config`. This file contains the entire kernel configuraton file, this will later be found in the microsdcard `/boot/config-`.

```
gary@petlab-P10S-WS:~/buildroot-sr-imx8/buildroot$ find . -name .config
./.config
./output/build/uboot-v2018.11-solidrun/.config
./output/build/busybox-1.29.3/.config
./output/build/linux-linux-4.14.y-nxp/.config
./output/build/uclibc-1.0.31/.config
```

In addition, we also need to modify Magic Version in the kernel MakeFile so that we can reuse the kernel module compiled in previous Debian rootfs that uses `4.14.104-imx8-sr` suffix. Otherwise the kernel that we compiled will be 4.14.104 and `modprobe` will complain about Magic Version mismatch (https://linux.die.net/lkmpg/x380.html). Kernel module information is checked using `modinfo <module_name>`

example
```
gary@sr-imx8:~$ sudo modinfo spidev
filename:       /lib/modules/4.14.104-imx8-sr/kernel/drivers/spi/spidev.ko
alias:          spi:spidev
license:        GPL
description:    User mode SPI device interface
author:         Andrea Paterniani, <a.paterniani@swapp-eng.it>
alias:          of:N*T*Csemtech,sx1301C*
alias:          of:N*T*Csemtech,sx1301
alias:          of:N*T*Cge,achcC*
alias:          of:N*T*Cge,achc
alias:          of:N*T*Clineartechnology,ltc2488C*
alias:          of:N*T*Clineartechnology,ltc2488
alias:          of:N*T*Crohm,dh2228fvC*
alias:          of:N*T*Crohm,dh2228fv
depends:
intree:         Y
name:           spidev
vermagic:       4.14.104-imx8-sr SMP preempt mod_unload aarch64
parm:           bufsiz:data bytes in biggest supported SPI message (uint)
```


```
nano ./output/build/linux-linux-4.14.y-nxp/Makefile
```

Add the `-imx8-sr` string in `EXTRAVERSION`. This will then later shows up in `uname -r`.
```
# SPDX-License-Identifier: GPL-2.0
VERSION = 4
PATCHLEVEL = 14
SUBLEVEL = 104
EXTRAVERSION = -imx8-sr
NAME = Petit Gorille

# *DOCUMENTATION*
# To see a list of typical targets execute "make help"
# More info can be located in ./README
# Comments in this file are targeted only to the developer, do not
# expect to learn how to build the kernel reading this file.

# That's our default target when none is given on the command line
PHONY := _all
_all:
...
```

Kernel info after loading.
```
gary@sr-imx8:~$ uname -r
4.14.104-imx8-sr
```

After modifying the kernel, the `.config` that contains the kernel configuration is stored in `output/build/uboot-v2018.11-solidrun/.config`. Invoke buildroot `menuconfig` again and tell it to use our custom `.config` instead.

```
make menuconfig
```

Navigate to `Kernel` and under `Kernel Configuration (Using a custom (def)config file)` option paste the path to the `.config`.
```
~/buildroot-sr-imx8/buildroot/output/build/linux-linux-4.14.y-nxp/.config
```

Rebuild the kernel
```
make
```

`make` will generate the kernel, rootfile system, u-boot, and package them into an image file flashable to microsd card. The image output is at `/buildroot-sr-imx8/buildroot/output/images`.

```
gary@petlab-P10S-WS:~/buildroot-sr-imx8/buildroot/output/images$ ls -alh
total 229M
drwxr-xr-x 2 root root 4.0K  九  25 16:14 .
drwxrwxr-x 7 gary gary 4.0K  九  25 16:14 ..
-rwxr-xr-x 1 root root  46K  九  12 18:51 bl31.bin
-rw-r--r-- 1 root root 985K  九  12 18:51 flash.bin
-rw-r--r-- 1 root root  41K  九  25 16:14 fsl-imx8mq-hummingboard-pulse.dtb
-rw-r--r-- 1 root root  17M  九  25 16:14 Image
-rw-r--r-- 1 root root 1.7K  九  12 18:51 lpddr4_pmu_train_1d_dmem.bin
-rw-r--r-- 1 root root  32K  九  12 18:51 lpddr4_pmu_train_1d_imem.bin
-rw-r--r-- 1 root root 1.4K  九  12 18:51 lpddr4_pmu_train_2d_dmem.bin
-rw-r--r-- 1 root root  23K  九  12 18:51 lpddr4_pmu_train_2d_imem.bin
-rw-r--r-- 1 root root 120M  九  25 16:14 rootfs.ext2
lrwxrwxrwx 1 root root   11  九  25 16:14 rootfs.ext4 -> rootfs.ext2
-rw-r--r-- 1 root root  45M  九  25 16:14 rootfs.tar
-rw-r--r-- 1 root root 122M  九  25 16:14 sdcard.img
-rw-r--r-- 1 root root 103K  九  12 18:51 signed_hdmi_imx8m.bin
```

To flash the image file, simply
```
dd if=sdcard.img of=/dev/sd<id_of_microsd>
```

After the initial `make`, any future changes to the kernel might require rebuilding the Image via the following. Only execute the following if the date of the `Image` file doesn't change after `make`.

Force recompile kernel
```
sudo make linux-rebuild all
```

# Update The Kernel Only
Usually one would simply use `dd` to flash the sdcard.img into a microsd card to boot the system. Since we only want to replace the kernel and not the root file system, we simply copy the generated `Image` file into `/boot/config`. Make sure to rename as `vmlinuz-4.14.104.imx8-sr` since that is the file name that the `boot/boot.scr` uboot script refer to during kernel booting.

```
scp buildroot/output/images/Image <username>@<IMX8-IP>:/home/<username>/boot/config/vmlinuz-4.14.104.imx8-sr
```

From iMX8 host OS, copy the Image to the `/boot` folder as root.
```
sudo cp ~/vmlinuz-4.14.104.imx8-sr /boot/
```

Note: Why don’t we just use the root file system built by buildroot?
the root file system provided by buildroot is very minimal and do not have package manager, it is meant to be statically build and those program had to be compiled during the build process. 
@ref: https://buildroot.org/downloads/manual/manual.html#faq-no-binary-packages

Example of U-Boot script that refer to the kernel image file `vmlinuz-4.14.104-imx8-sr`.

```
nano /boot/boot.scr
```

```
#
# flash-kernel: bootscr.uboot-generic
#

# Bootscript using the new unified bootcmd handling
#
# Expects to be called with the following environment variables set:
#
#  devtype              e.g. mmc/scsi etc
#  devnum               The device number of the given type
#  bootpart             The partition containing the boot files
#  distro_bootpart      The partition containing the boot files
#                       (introduced in u-boot mainline 2016.01)
#  prefix               Prefix within the boot partiion to the boot files
#  kernel_addr_r        Address to load the kernel to
#  fdt_addr_r           Address to load the FDT to
#  ramdisk_addr_r       Address to load the initrd to.
#
# The uboot must support the booti and generic filesystem load commands.

if test -n "${console}"; then
  setenv bootargs "${bootargs} console=${console}"
fi

setenv bootargs  ${bootargs} log_level=7 net.ifnames=0


if test -z "${fk_kvers}"; then
   setenv fk_kvers '4.14.104-imx8-sr'
fi

# These two blocks should be the same apart from the use of
# ${fk_kvers} in the first, the syntax supported by u-boot does not
# lend itself to removing this duplication.

if test -n "${fdtfile}"; then
   setenv fdtpath dtbs/${fk_kvers}/${fdtfile}
else
   setenv fdtpath dtb-${fk_kvers}
fi

if test -z "${distro_bootpart}"; then
  setenv partition ${bootpart}
else
  setenv partition ${distro_bootpart}
fi



load ${devtype} ${devnum}:${partition} ${kernel_addr_r} ${prefix}vmlinuz-${fk_kvers} \
&& load ${devtype} ${devnum}:${partition} ${fdt_addr_r} ${prefix}${fdtpath} \
&& load ${devtype} ${devnum}:${partition} ${ramdisk_addr_r} ${prefix}initrd.img-${fk_kvers} \
&& echo "Booting Debian ${fk_kvers} from ${devtype} ${devnum}:${partition}..." \
&& booti ${kernel_addr_r} ${ramdisk_addr_r}:${filesize} ${fdt_addr_r}

load ${devtype} ${devnum}:${partition} ${kernel_addr_r} ${prefix}vmlinuz \
&& load ${devtype} ${devnum}:${partition} ${fdt_addr_r} ${prefix}dtb \
&& load ${devtype} ${devnum}:${partition} ${ramdisk_addr_r} ${prefix}initrd.img \
&& echo "Booting Debian from ${devtype} ${devnum}:${partition}..." \
&& booti ${kernel_addr_r} ${ramdisk_addr_r}:${filesize} ${fdt_addr_r}
```

# Optional: Modify U-Boot script
@ref: https://wiki.ubuntu.com/ARM/EditBootscr

Requirements
```
sudo apt install u-boot-tools uboot-mkimage
```

Strip the 72byte long uboot header:
```
dd if=boot.scr of=boot.script bs=72 skip=1
```

Make your changes and rebuild the script.
```

mkimage -A arm64 -T script -C none -n "Ubuntu boot script" -d boot.script boot.scr
```

# Optional: Compile the kernel module only
The following assume that cross compile is done on a Ubuntu system.

Get the kernel header source
```
sudo apt install linux-4.14.104
```

Example of compiling the netfilter module.
```
~/buildroot-sr-imx8-modules-compile-test/buildroot/output/build/linux-linux-4.14.y-nxp$ sudo make ARCH=arm64 CROSS_COMPILE=/home/gary/imx8-dev/toolchain/gcc-linaro-7.3.1-2018.05-i686_aarch64-linux-gnu/bin/aarch64-linux-gnu- -C . M=net/netfilter
```


# Appendix
**Location of the kernel config boot file**
```
/boot/config-
```

**Location of kernel modules loaded into the kernel**
```
/sys/modules
```

Note that the contents of `/lib/modules` might not be in sync with `/sys/modules`

**View the current running kernel config**
```
cat /proc/config.gz | gunzip > running.config
```

This is a preferred way to view the current kernel config since the config file in `/boot/config` might be overwritten or outdated.


